let editForm = document.querySelector("#editCourse");
let params = new URLSearchParams(window.location.search);
console.log(params);
let courseid= params.get('courseId');
console.log(courseid);
let token = localStorage.getItem('token');

editForm.addEventListener("submit", (e)=>{
    
      e.preventDefault(); // if nagerror di babalik yung form sa orig state
      
      let courseName = document.querySelector("#courseName").value;
      let coursePrice =document.querySelector('#coursePrice').value
      let courseDescription =document.querySelector('#courseDescription').value
      
      console.log(courseName);
      console.log(coursePrice);
      console.log(courseDescription);


      if(courseName == "" || coursePrice == "" || courseDescription=="") {
      	alert("Please complete the information");
      } else {
       
        fetch(`https://cryptic-hollows-03197.herokuapp.com/api/courses`,{
        	method: 'PUT',
           headers: {
          'Content-Type': 'application/json',
 	        Authorization: `Bearer ${token}`
            },
            body: JSON.stringify({
            name: courseName,
       		description: courseDescription,
       		price: coursePrice,
       		courseId : courseid


       	   })
          })
        .then(res => res.json())
		    .then(data => {

		     console.log(data);

         window.location.replace("courses.html");
         
      
       
         
 
      		

       })

     }
 })


